#!/bin/bash

tileSize[0]=12
tileSize[1]=5
#ileSize[1]=10
tileSize[2]=2
tileSize[3]=1

highestAdm=2

noTiles=()

for (( level = 1; level <= $highestAdm; level++ )); do
  # progress output
  echo "**************************"
  echo "creating for $level"
  echo "**************************"
  i=0

  for (( long = 0; long < 360; long=$((long + tileSize[level])) )); do
    # create the real value of the longitude and the longitude end
    longReal=$((long - 180))
    longEnd=$((longReal + tileSize[level]))

    #run variable for notilelist
    i=$((i+1))
    j=0

    # progress output
    echo "*************$level $longReal*************"

    for (( lat = 0; lat < 180; lat=$((lat + tileSize[level])) )); do
      # create the real value of the latitude and the latitude end
      latReal=$((lat - 90))
      latEnd=$((latReal + tileSize[level]))

      # # progress output
      echo "$level $longReal $latReal"

      # create a temp shapefile from the given bounding box
      pgsql2shp -f temp_adm${level}_${long}_${lat} d3choropleth "SELECT * from gadm${level}_simple WHERE ST_Centroid(geom) && ST_MakeEnvelope($longReal, $latReal, $longEnd, $latEnd, 4326)" > /dev/null 2>&1

      # create a reference to the selected ids into the gadm_id_tile_ref table
      psql d3choropleth -c "INSERT INTO gadm_id_tile_ref(id, tile_long, tile_lat) SELECT id, $long, $lat from gadm${level}_simple WHERE ST_Centroid(geom) && ST_MakeEnvelope($longReal, $latReal, $longEnd, $latEnd, 4326);"

      # create a notile list, with tiles that dont need to be loaded (because there is nothing in them)
      num=$(psql d3choropleth -c "SELECT count(r.id) from gadm${level}_simple as s join gadm_id_tile_ref as r on s.id = r.id WHERE geom && ST_MakeEnvelope($longReal, $latReal, $longEnd, $latEnd, 4326);")
      if [ `echo $num | awk '{print $3==0}'` -eq 1 ]; then
        noTiles+=('{"level":'$level',"long":'$i',"lat":'$j'}')
      fi

      if [ -f temp_adm${level}_${long}_${lat}.shp ]; then
        # create topojson files from the shape files
        mapshaper -i temp_adm${level}_${long}_${lat}.shp -rename-layers layer -rename-fields NAME=NAME_${level} -o format=topojson out/adm$level/adm${level}_${long}_${lat}.json > /dev/null 2>&1

        # add attributes to identify this tiles
        sed -i "$ s/.$/,\"level\":$level,\"long\":$longReal,\"lat\":$latReal}/" out/adm$level/adm${level}_${long}_${lat}.json
      fi
      # remove temp files
      rm temp_adm*

    done
  done
done

# create the inittiles.json
x=$(IFS=,; echo "${noTiles[*]}")

echo '{"noTiles":['$x']}' > out/initTiles.json
