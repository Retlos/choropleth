import sys

import csv

with open("adm" + sys.argv[1] + ".csv") as f:
    with open("adm" + sys.argv[1] + "_out.csv", "w") as f2:
        reader = csv.reader(f)
        writer = csv.writer(f2, lineterminator='\n')
        row = reader.__next__()
        row.append("ID")

        new = []
        new.append(row)

        for row in reader:
            if(sys.argv[1] == "0"):
                row.append(row[1])
            elif(sys.argv[1]=="1"):
                row.append(row[1]+"_"+row[4])
            elif(sys.argv[1]=="2"):
                row.append(row[1]+"_"+row[4]+"_"+row[6])
            else:
                print(sys.argv[1])
            new.append(row)

        writer.writerows(new)

