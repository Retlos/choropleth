# from tornado.ioloop import IOLoop
import tornado.web
import tornado.websocket
from tornado.platform.asyncio import AsyncIOMainLoop, to_tornado_future

import asyncio
import aiopg

import psycopg2
import psycopg2.extras


import json

import os

ioloop = None
root = os.path.dirname(__file__)


class Database:
  pool = None
  async def init():
    Database.pool = await aiopg.create_pool("dbname=d3choropleth user=dworschak password=A2C4H8A1U host=127.0.0.1", minsize=10, maxsize=20)

class ErrorHandler:
  def sendError(socket, error):
    message = {
      "error": True,
      "message": error
    }
    socket.write_message(json.dumps(message))

class MapHandler(tornado.websocket.WebSocketHandler):
  # the connection is established
  def open(self):
    pass

  async def mapQuery(self, message):
    try:
      outData = []
      async with Database.pool.acquire() as conn:
        async with conn.cursor() as cur:
          for tile in message["tiles"]:
            right = tile["rect"]["right"]
            left = tile["rect"]["left"]
            top = tile["rect"]["top"]
            bottom = tile["rect"]["bottom"]
            zoomlevel = tile["rect"]["level"]


            await cur.execute("""
                SELECT ARRAY_AGG(DISTINCT CONCAT_WS('_',id_0,id_1,id_2,id_3))
                FROM gadm_bbox
                WHERE level = %(zoomlevel)s
                  AND latrange && numrange(%(bottom)s, %(top)s)
                  AND longrange && numrange(%(left)s, %(right)s);
                """, {"zoomlevel": zoomlevel, "right": right, "left": left, "top": top, "bottom": bottom})

            res = [el for el in cur]

            if res[0][0] == None:
              idList = None
            else:
              # remove the Caspian Sea and Antarctica from the list --> both would only cause problems
              idList = [ID for ID in res[0][0] if ID != 10 and ID != 44]

            geoList = []

            if(idList == None):
              idList = []
            else:
              await cur.execute("""
                  SELECT ARRAY_AGG(DISTINCT CONCAT_WS('_',tile_long, tile_lat))
                  FROM gadm_id_tile_ref
                  WHERE id IN %s;
                  """, (tuple(idList), ))

              geoList = [el for el in cur][0][0]


            outData.append({
              "index": tile["index"],
              "data": {
                "contents": idList,
                "geoTiles":geoList
                }
              })

      self.write_message(json.dumps({"tiles": outData, "taskRef": message["taskRef"]}))

    except Exception as e:
      print("Error", e)
      ErrorHandler.sendError(self, "An error occured on the server")


  # recieve data from the socket
  def on_message(self, message):
    m = json.loads(message)

    asyncio.async(self.mapQuery(m))


  # connection is closed
  def on_close(self):
    pass


class RecordHandler(tornado.websocket.WebSocketHandler):
  # create vars to get the correct admin ids
  concatADM = [
    "rg.gadm2_ids[1]",
    "(rg.gadm2_ids[1] || '_' || rg.gadm2_ids[2])",
    "(rg.gadm2_ids[1] || '_' || rg.gadm2_ids[2] || '_' || rg.gadm2_ids[3])"
    ]
  concatADMGlobal = [
    "gadm2_ids[1]",
    "(gadm2_ids[1] || '_' || gadm2_ids[2])",
    "(gadm2_ids[1] || '_' || gadm2_ids[2] || '_' || gadm2_ids[3])"
    ]
  groupBy = [
    "rg.gadm2_ids[1]",
    "rg.gadm2_ids[1], rg.gadm2_ids[2]",
    "rg.gadm2_ids[1], rg.gadm2_ids[2], rg.gadm2_ids[3]"
    ]

  # the connection is established
  def open(self):
    asyncio.async(self.initHandler())

  # convert id strings to id arrays
  def convertIDStringsToArrays(ids):
    arrayIDs = []
    for idElement in ids:
      arrayID = RecordHandler.convertIDStringToArray(idElement)
      arrayIDs.append(arrayID)
    return arrayIDs

  def convertIDStringToArray(id):
    arrayID = [0,0,0,0,0,0]
    tempArrayID = id.split("_")
    for i in range(tempArrayID.__len__()):
      arrayID[i] = tempArrayID[i]
    return arrayID

  async def getCachedData(ids, filterString, level):
    # get filter id (if not exists insert filter and return new id) and use it to get all valid cached elements
    # http://stackoverflow.com/questions/18192570/insert-if-not-exists-else-return-id-in-postgresql
    sqlFilterID = """
      WITH filterselect AS (
        SELECT id
        FROM record_gadm2_cache_filter
        WHERE filter @> %(filter)s::JSONB and filter <@ %(filter)s::JSONB
    ), filterinsert AS (
        INSERT INTO record_gadm2_cache_filter ("filter")
        SELECT %(filter)s::JSONB
        WHERE NOT EXISTS (SELECT 1 FROM filterselect)
        RETURNING id
    )
    SELECT """ + RecordHandler.concatADMGlobal[level] + """ id, value n
    FROM record_gadm2_grouped_cache
    WHERE
      filter_id = (
        SELECT id
        FROM filterinsert
        UNION ALL
        SELECT id
        FROM filterselect
      )
      AND gadm2_ids IN %(idList)s
      AND created + INTERVAL '100 days' > current_timestamp;
    """

    dataFilterID = {"idList": tuple(ids), "filter": filterString}

    #RealDictCursor returns a result with dict values
    async with Database.pool.acquire() as conn:
      async with conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor) as cur:

        # print(await cur.mogrify(sql, data))

        await cur.execute(sqlFilterID, dataFilterID)

        return [el for el in cur]

  async def updateCache(cur, res, filterString):
    # convert id strings to id arrays
    updateValueTuples = [(RecordHandler.convertIDStringToArray(r["id"]), r["n"]) for r in res]
    updateValueString = ','.join(['%s'] * len(updateValueTuples))

    dataUpdate = updateValueTuples + [filterString, filterString]

    sqlUpdate = """
      WITH new_values(gadm2_ids, value) AS (
        VALUES
         """ + updateValueString + """
      ), filterselect AS (
        SELECT id
        FROM record_gadm2_cache_filter
        WHERE filter @> %s::JSONB and filter <@ %s::JSONB
      ), updatequery AS (
        UPDATE record_gadm2_grouped_cache g
        SET
          gadm2_ids = nv.gadm2_ids,
          value = nv.value,
          created = current_timestamp
        FROM new_values nv
        WHERE
          g.filter_id = (SELECT id FROM filterselect)
          AND g.gadm2_ids = nv.gadm2_ids
        RETURNING g.gadm2_ids, g.filter_id
      )
      INSERT INTO record_gadm2_grouped_cache(filter_id, gadm2_ids, value)
      SELECT (SELECT id FROM filterselect) id, gadm2_ids, value
      FROM new_values
      WHERE NOT EXISTS (
        SELECT 1
        FROM updatequery
        WHERE
          updatequery.gadm2_ids = new_values.gadm2_ids
          AND updatequery.filter_id = (SELECT id FROM filterselect)
      );
    """

    # print(await cur.mogrify(sqlUpdate, dataUpdate))
    await cur.execute(sqlUpdate, dataUpdate)

  async def initHandler(self):
    try:
      #RealDictCursor returns a result with dict values
      async with Database.pool.acquire() as conn:
        async with conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor) as cur:

          await cur.execute("""SELECT name1, id1, name2, id2, array_agg(id3||','|| name3) name3list
                               FROM classif_labels
                               GROUP BY name1, id1, name2, id2
                               ORDER BY name1 ASC, name2 ASC;""")

          res = [el for el in cur]

          self.write_message(json.dumps({"init": True, "sm": res}))
    except Exception as e:
      print("Error:", e)
      ErrorHandler.sendError(self, "An error occured on the server")
      self.write_message(errorMessage)

  async def recQuery(self, m):
    try:
      if m["level"] != 0 and m["level"] != 1 and m["level"] != 2:
        ErrorHandler.sendError(self, "Admin level not supported")
        return


      # convert id strings to id arrays
      arrayIDs = RecordHandler.convertIDStringsToArrays(m["ids"])

      filterString = json.dumps(m["filter"])

    # get the cached Data
      resCache = await RecordHandler.getCachedData(arrayIDs, filterString, m["level"])

      # remove successfully queried elements from the idlist
      for el in resCache:
        m["ids"].remove(str(el["id"]))

      # if every id was loaded from cache return values
      if m["ids"].__len__() == 0:
        self.write_message(json.dumps({"level": m["level"], "taskRef": m["taskRef"], "recData": resCache}))
        return

      # create filter vars
      data = {"tupleIdList": tuple(m["ids"]), "idArray": m["ids"]}
      queryFilter = ""
      filterJoin = ""

      if m["filter"].__len__() > 0:
        fcopub = m["filter"].get("copub")

        if fcopub != None:
          # remove copub from filter --> so that later it can be checked if copub was the only filter
          #   neccessary cause otherwise it will ad an join with the record table
          del m["filter"]["copub"]

          copubLevel = fcopub.count("_")
          queryFilter += " AND " + RecordHandler.concatADM[copubLevel] + " != %(copubID)s "
          data["copubID"] = fcopub

          filterJoin += """
          RIGHT JOIN
            (
              SELECT record_id
              FROM record_gadm2
              WHERE """ + RecordHandler.concatADMGlobal[copubLevel] + """ = %(copubID)s
            ) copub
          ON rg.record_id = copub.record_id
          """

        if m["filter"].__len__() > 0:
          filterJoin += " JOIN record r ON rg.record_id = r.id"
          fYear = m["filter"].get("year")
          fTimesCited = m["filter"].get("timesCited")
          fCitable = m["filter"].get("citable")
          fSm = m["filter"].get("sm")
          fCopus = m["filter"].get("id_scopus")
          fWos = m["filter"].get("id_wos")
          fAuthorCount = m["filter"].get("authorCount")
          finvolvedCountries = m["filter"].get("involvedCountries")

          if fYear != None and fYear.get("start") != None and fYear.get("end") != None:
            queryFilter += " AND (r.year BETWEEN %(yearStart)s AND %(yearEnd)s) "
            data["yearStart"] = fYear["start"]
            data["yearEnd"] = fYear["end"]

          if fTimesCited != None and fTimesCited.get("start") != None and fTimesCited.get("end") != None:
            queryFilter += " AND (r.timescited_corr BETWEEN %(citedStart)s AND %(citedEnd)s) "
            data["citedStart"] = fTimesCited["start"]
            data["citedEnd"] = fTimesCited["end"]

          if fCitable != None:
            queryFilter += " AND (r.citable=%(citable)s) "
            data["citable"] = fCitable

          if fSm != None and fSm.__len__() > 0:
            queryFilter += " AND (r.sm_subfield IN %(sm_subfields)s) "
            data["sm_subfields"] = tuple(fSm)

          if fCopus != None:
            queryFilter += " AND (id_scopus is not NULL) "

          if fWos != None:
            queryFilter += " AND (id_wos is not NULL) "

          if fAuthorCount != None and fAuthorCount.get("start") != None and fAuthorCount.get("end") != None:
            queryFilter += " AND (r.author_count BETWEEN %(authorCountStart)s AND %(authorCountEnd)s) "
            data["authorCountStart"] = fAuthorCount["start"]
            data["authorCountEnd"] = fAuthorCount["end"]

          if finvolvedCountries != None and finvolvedCountries.get("start") != None and finvolvedCountries.get("end") != None:
            queryFilter += " AND (array_length(r.cc, 1) BETWEEN %(finvolvedCountriesStart)s AND %(finvolvedCountriesEnd)s) "
            data["finvolvedCountriesStart"] = finvolvedCountries["start"]
            data["finvolvedCountriesEnd"] = finvolvedCountries["end"]


      sql = """
        SELECT
          i.id id,
          COALESCE(rg.n, 0) n
        FROM
          (
            SELECT unnest( %(idArray)s ) id
          ) i
          LEFT JOIN
          (
            SELECT
              COUNT(DISTINCT(rg.record_id)) n,
              """ + RecordHandler.concatADM[m["level"]] + """ adm
            FROM
              record_gadm2 rg
              """ + filterJoin + """
            WHERE """ + RecordHandler.concatADM[m["level"]] + """ IN %(tupleIdList)s """ + queryFilter + """
            GROUP BY """ + RecordHandler.groupBy[m["level"]] + """
          ) rg
          ON i.id = (rg.adm::text);
      """

      #RealDictCursor returns a result with dict values
      async with Database.pool.acquire() as conn:
        async with conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor) as cur:

          # print(await cur.mogrify(sql, data))

          await cur.execute(sql, data)

          res = [el for el in cur]

          self.write_message(json.dumps({"level": m["level"], "taskRef": m["taskRef"], "recData": res + resCache}))

          # update / insert into cache
          await RecordHandler.updateCache(cur, res, filterString)

    except Exception as e:
      print("Error:", e)
      ErrorHandler.sendError(self, "An error occured on the server")

  # recieve data from the socket
  def on_message(self, message):
    m = json.loads(message)

    asyncio.async(self.recQuery(m))

  # connection is closed
  def on_close(self):
    pass



def make_app():
    return tornado.web.Application([
            (r"/mapData", MapHandler),
            (r"/recordData", RecordHandler),
            (r"/(.*)", tornado.web.StaticFileHandler, {'path': root.join("client"), "default_filename": "index.html"}),
    ],debug=True, compress_response=True) #TODO: remove debug=True in production

if __name__ == "__main__":

  AsyncIOMainLoop().install()
  ioloop = asyncio.get_event_loop()
  ioloop.run_until_complete(Database.init())

  app = make_app()
  app.listen(8003)

  ioloop.run_forever()


