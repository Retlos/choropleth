//todo make zoom and zoom levels independent of viewport size
c = (function(){
  var choro = {};
  // basic variables that are often used
    // width and height are mostly use to calculate the center and to calculate the bounding box
    // center is used to translate the map to the correct center and to calculate the zoomclick
    // uim and tm hold the UserInterfaceManager and the TileManager, which hold important functions for their field
  var width = window.innerWidth || document.body.clientWidth,
      height = window.innerHeight || document.body.clientHeight,
      center = [width / 2, height / 2];

  var zoom, projection, path;

  // general settings for the behaviour of the application
  choro.settings = {
    datafolder: "data/", // folder where the data is being stored (eg. map data)
    map:{
      highestAdminLevel: 2, // defines how many admin levels exist (if more than 2 is choosen, than shapes for admin level 3 must be provided int he appropriate folder)
      center: [13.3, 47.80], // defines the initial center
      initialZoom: 150, // defines the initial zoom
      zoomlevels: [20, 60, 120, Infinity], // defines the values when an admin level change occurs
      tileSize: [12, 5, 2, 1], // defines the tilesizes of the different admin levels (in long/lat coordinates) // a tile is always square
      stopLevelChanges: false // option to disable administration level changes
    },
    ui:{
      info: true, // displays info about a country (eg name + num of records)
      zoomfactor: [1.2, 1/1.2], // speed of zooming with ui buttons
      panfactor: 50, // speed of panning with ui buttons
      zoomExtant: [0.5, 300] // the extant of the zoom
    },
    data:{
      scaleMin: 0, // min value of the scale // everything below will be truncated // only works if updateScale is false
      scaleMax: 1000000, // max value of the scale // everything above will be truncated // only works if updateScale is false
      colorScale: 0, // defines the color scale that is beeing used // 0 blue // 1 green // 2 red // 3 black
      updateScale: false, // if the scale updates permanently or is persistent with the current values
      scaleType: 0, // the initial type of the scale //0 --> exponential, 1 linear
      useLocalstorage: true, // setting that allows disabling the use of localstorage
      startTime: 0, // for localstorage expiration // will be set programmatically at choro.init
      timeToExpire: 1000 * 60 * 60 * 24 * 100, //in ms / time until data in the localstorage expires //currently expires in 100 days
      recordDataSocket: "ws://localhost:8003/recordData", // websocket of publication data
      mapDataSocket: "ws://localhost:8003/mapData", // websocket of mapdata (for ids and refs which tiles to load)
      websocketReconnection: 30000 // in ms / if the websocketconnection wasnt established/is interrupted --> the websocket tries to reconnect in this time
    },
    filter:{
      yearmin: 1930, // minvalue for the year filter
      yearmax: -1, // maxvalue of the year filter // if -1 it is set to current year
      citationMin: 0,// minvalue for the citation filter
      citationMax: 20000,// maxvalue of the citation filter
      authorMin: 0,// minvalue for the author filter
      authorMax: 10000,// maxvalue of the author filter
      involvedCountriesMin: 0,// minvalue for the involvedCountries filter
      involvedCountriesMax: 255// maxvalue of the involvedCountries filter
    },
    debug:{
      newDataStop: false, // disables new data from loading // allows to see what is currently displayed on the map
      debugMode: false // enables/disables debug functionality and debug information
    }
  };

  // objects that defines which features are supported by the browser or not
  // is filled in the choro.init method
  var support = {};

  // initializes this application
  // returns the choro object, which holds all public vars of this application
  choro.init = function(){

    // check browser support for vectoreffects (used for automatically scaling strokes)
    support.vectorEffect = function(){
      var elm = document.createElementNS("http://www.w3.org/2000/svg", "g");
      return elm.style.vectorEffect !== undefined;
    }();

    // localstorage support test (from modernizr)
    support.localstorage = function(){
      var test = "supportTest";
      try{
        localStorage.setItem(test, test);
        localStorage.removeItem(test);
        return true;
      }catch(e){
        choro.settings.data.useLocalstorage = false;
        return false;
      }
    }();

    // get the current year (neccessary for caching)
    choro.settings.data.startTime = new Date().getTime();


    // creates a difference method for Arrays
    // returns an Array of Elements that are in Array one but not in Array two
    // problems with ie older than 9 (doesnt know .filter())
    Array.prototype.diff = function(a){
      return this.filter(function(item){return a.indexOf(item) < 0;});
    };

    // creates a unique method for arrays
    // returns unique elements
    // problems with ie older than 9 (doesnt know .filter())
    Array.prototype.unique = function(){
      self = this;
      return this.filter(function(item, pos){return self.indexOf(item) == pos;});
    };

    // splits all elements of an array with the given divider
    // elements in the array should be strings
      // if they are numbers or arrays it will not work, but wont return an error
      // if they are objects {}, they will return an error
    // input: divider --> string that divides each element; index (optional), index of the splitted element which will be returned, if undefined it will return everything
    // returns: array with same length as given array, with the splitted elements as elements
    Array.prototype.split = function(divider, index){
      var ret = [];
      if (typeof index === 'undefined'){
        this.forEach(function(item){item = item + ""; ret.push(item.split(divider));});
      }else{
        this.forEach(function(item){item = item + ""; ret.push(item.split(divider)[index]);});
      }
      return ret;
    };

    // create Manager Objects that handle the behaviour of the application
    choro.uiManager = new UIManager();
    choro.tileManager = new TileManager();
    choro.displayManager = new DisplayManager();
    choro.connectionManager = new ConnectionManager();
    choro.dataManager = new DataManager();

    // creates the correct map projection and centers it on a given point with a given zoom level
    projection = d3.geo.mercator()
        .translate([1280 / 2, 700 / 2])
        .center(choro.settings.map.center)
        .scale(choro.settings.map.initialZoom);

    // create zoom variable to use d3s zoom behavior
    zoom = d3.behavior.zoom()
      .scale(5) //scale and zoom to europe
      .translate([-3232.204514891994 + center[0],-1755.401371921605 + center[1]]) //TODO
      .scaleExtent(choro.settings.ui.zoomExtant)
      .on("zoom", choro.displayManager.zoomMap)
      .on("zoomstart", choro.uiManager.hideInfo)
      .on("zoomend", choro.displayManager.zoomMapEnd);

    // generates the correct path function for the given projection
    path = d3.geo.path()
        .projection(projection);

    //load order --> displayManager before uiManager, before tileManager before connectionManager
      //displayManager.init initializes svg element, which is needed by uiManager.init
      //uiManager.init initializes loadingScreen, which is used by connectionManager.init
      //tileManager.init creates the tiles which is needed by the connectionManager.init
    choro.displayManager.init();
    choro.uiManager.init();
    choro.connectionManager.init();
    choro.dataManager.init();
    choro.tileManager.init();

    return choro;
  };



  // changes width, height and center, if the browser resized
  window.onresize = function(){
    width = window.innerWidth || document.body.clientWidth;
    height = window.innerHeight || document.body.clientHeight;
    center = [width / 2, height / 2];

    // check if something has changed
    choro.tileManager.checkTiles();
  };

  // accesses datamanager, d3, uiManager
  // visible to public: init(), displayMap(), zoomMap(), colorizeMap()
  // accesses TileManager, topojson, d3, uiManager, zoom
  function DisplayManager(){
    var map, svg;
    var colorStylesheet;

    var colorValues = [
        //blue
        ['rgb(247,251,255)','rgb(222,235,247)','rgb(198,219,239)','rgb(158,202,225)','rgb(107,174,214)','rgb(66,146,198)','rgb(33,113,181)','rgb(8,81,156)','rgb(8,48,107)'],        //green
        //green
        ['rgb(247,252,245)','rgb(229,245,224)','rgb(199,233,192)','rgb(161,217,155)','rgb(116,196,118)','rgb(65,171,93)','rgb(35,139,69)','rgb(0,109,44)','rgb(0,68,27)'],
        //red
        ['rgb(255,245,240)','rgb(254,224,210)','rgb(252,187,161)','rgb(252,146,114)','rgb(251,106,74)','rgb(239,59,44)','rgb(203,24,29)','rgb(165,15,21)','rgb(103,0,13)'],
        //black
        ['rgb(255,255,255)','rgb(240,240,240)','rgb(217,217,217)','rgb(189,189,189)','rgb(150,150,150)','rgb(115,115,115)','rgb(82,82,82)','rgb(37,37,37)','rgb(0,0,0)']
      ];

    var copubColor = "rgb(255,255,0)";

    // quantize variable that can split the input variable into a discrete scale
    var quantize = d3.scale.quantize()
      .domain([0, 9]);

    // initializes the displaymanager
    // creates svg element and group elements
    this.init = function(){
      // append svg Element to body and provide the right height and width
      svg = d3.select("body").append("svg")
          .attr("width", "100%")
          .attr("height", "100%")
          .attr("class", "Blues")
          .call(zoom)
          .on("dblclick.zoom", null);

      // get the new stylesheets just for the color
      colorStylesheet = document.getElementById("colorStyle").sheet;

      // set initial colorscale values
      this.changeColorScale();

      // append the group element which will hold the map
      // this group is also used to pan/zoom the map
      map = svg.append("g").attr("id", "map");

      // append groups that hold the different admin levels
      map.append("g").attr("class", "lAdmLevel admlevel adm" + 0).attr("data-level", 0);
      map.append("g").attr("class", "lAdmLevel admlevel adm" + 1).attr("data-level", 1);
      map.append("g").attr("class", "lAdmLevel admlevel adm" + 2).attr("data-level", 2);
      map.append("g").attr("class", "lAdmLevel admlevel adm" + 3).attr("data-level", 3);
      map.append("g").attr("id", "debug");

      if(!support.vectorEffect){
        d3.selectAll(".boundary").style("stroke-width", 1 / zoom.scale() + "px");
      }
      map.attr("transform", "translate(" + zoom.translate() + ")scale(" + zoom.scale() + ")");

      this.changeLevel(0);
    };

    // changes the color of the colorscale legend and the quantize scale (which maps a value from 0-9 to the rgb color)
    this.changeColorScale = function(){
      quantize.range(d3.range(9).map(function(i){return colorValues[choro.settings.data.colorScale][i];}));

      for (var i = colorValues[choro.settings.data.colorScale].length - 1; i >= 0; i--) {
        document.getElementById("s"+i).getElementsByTagName("span")[0].style.background = colorValues[choro.settings.data.colorScale][i];
      }
    };

    // draws the shapes(countries, states, etc) on the svg element
    // input:
      // geoData: array of topojson objects
      // level: currently viewed administration level
    this.drawShapes = function(geoData, level) {

      // create a freature array from geoData
      var features = createFeatures(geoData);

      // appends the data to the boundaries
      // second attribute of data is usually optional. It defines a key function to search for it easier.
      // since there were some problems with overriden keys this function is no longer optional but a must
        //Problem: zoom in to adm1 to romania and pan to france --> france adm1 wasnt shown..
      var boundaries = svg.select(".adm" + level)
            .selectAll(".boundary")
              .data(features, getID);

      boundaries.enter().append("path")
          .attr({
            "d": path,
            "class":"boundary",
            "id": function(d){return "b" + getID(d);},
            "data-name": function(d){return getName(d, level);},
            "data-id": getID,
            "vector-effect": "non-scaling-stroke"
          })
          .on("click", choro.uiManager.displayInfo);

        if(!support.vectorEffect){
          d3.selectAll(".boundary").style("stroke-width", 1 / zoom.scale() + "px");
        }

    };

    // colorizes the map
    // input
      // recordData: array of {id, value} objects with log scaled values
      // scaleChanged: boolean --> if true than remove all previous styles from stylesheet
    this.colorizeMap = function(recordData, scaleChanged){
      // if(scaleChanged){ // todo find a solution...: problem if this is enabled is that every time the view moved the stylesheet increases in size..
         removeStyles();
      // }

      // get id and remove all "_0" occurences --> neccessary to display shapes with missing lower admin levels
      // eg. replaces adm2 id 16_0_0 with 16; this example happens if country with id 16 only has adm0
      var copubID = choro.uiManager.getCopubID() + "";
      copubID = copubID.replace(/_0/g, "");

      for (var i = 0; i < recordData.length; i++) {
        // get id and remove all "_0" occurences --> neccessary to display shapes with missing lower admin levels
        // eg. replaces adm2 id 16_0_0 with 16; this example happens if country with id 16 only has adm0
        var id = recordData[i].id.replace(/_0/g, "");
        var rule;

        if(copubID === id){
          rule = "#b" + id + "{fill: " + copubColor + " ";
        }else{
          rule = "#b" + id + "{fill: " + quantize(recordData[i].value) + ";} ";
        }

        colorStylesheet.insertRule(rule, 0);
      }

      choro.uiManager.stopLoading();
    };

    // removes all styles from the stylesheet
    function removeStyles(){
      for (var i = colorStylesheet.cssRules.length - 1; i >= 0; i--) {
        colorStylesheet.deleteRule(i);
      }
    }

    // organizes the display of the different admin groups
      // makes upper admin levels see through with a more dominant border --> state and country borders can be seen in lower levels
      // removes every path of lower levels --> its not needed right now so dont display it
    this.changeLevel = function(level){
      // add "higher adm level class" to the higher admin level
      map.select(".adm" + (level - 1)).classed("hAdmLevel", true);

      // remove "higher/lower adm level class" from the current admin level
      map.select(".adm" + level).classed("hAdmLevel", false);
      map.select(".adm" + level).classed("lAdmLevel", false);

      // add lover admin class to lower admin levels
      map.select(".adm" + (level + 1)).classed("lAdmLevel", true);
      map.select(".adm" + (level + 2)).classed("lAdmLevel", true);
    };

    //zooms and pans the map by using the zoom behaviour of d3
    this.zoomMap = function(){
      map.attr("transform", "translate(" + zoom.translate() + ")scale(" + zoom.scale() + ")");
    };

    // only gets called if the current zoom ended or when ui zoom is used
    // calls checkTiles to check if new tiles are beeing displayed
    // if vectoreffects is not activated it calculates the strokewidth for this zoom level
    this.zoomMapEnd = function(){
      choro.uiManager.hideInfo();

      choro.tileManager.checkTiles();
      if(!support.vectorEffect){
        d3.selectAll(".boundary").style("stroke-width", 1 / zoom.scale() + "px");
      }

      map.attr("transform", "translate(" + zoom.translate() + ")scale(" + zoom.scale() + ")");
    };

    // create topojson features from the geoData Argument
    // input: geoData either an Object or an Array of Objects
    // returns an Array with topojson features
    function createFeatures(geoData){
      var features;
      if(geoData.length === undefined){
        features = topojson.feature(geoData, geoData.objects.layer).features;
      }else{
        for (var i = 0; i < geoData.length; i++) {
          features = features.concat(topojson.feature(geoData[i], geoData[i].objects.layer).features);
        }
      }

      return features;
    }

    // returns an id String for a boundary object
    function getID(d){
      return d.properties.ID + "";
    }

    // returns the Name of the boundary object --> attributename depends on the administration level
    function getName(d, level){
      return d.properties.NAME_ISO || d.properties.NAME;
    }
  }

  // connects to the server and returns the data to the callback Methods
  // accesses server, WebSocket, JSON, QUEUE()
  // visible to public: init(), loadIds(), prepLoadData(), prepLoadGeo(), load()
  function ConnectionManager(){

    //websocket connections
    var sockets = [];

    // initializes the websocket connections with the server
    this.init = function(){
      // create socket connections
      choro.connectionManager.startSocket("records", choro.settings.data.recordDataSocket, choro.dataManager.mapRecordValues);
      choro.connectionManager.startSocket("map", choro.settings.data.mapDataSocket, choro.tileManager.cacheTiles);
    };

    // sends the tiles to the map socket
    // get map ids that are within the given tiles
    this.loadIds = function(tiles){
      sockets.map.send(tiles);
    };

    // get records of the given admin level
    // input:
      // idList -> array of ids
      // level -> the adminLevel
      // filter -> the filter that is beeing applied
      // taskRef --> reference to task --> used by the application to distinguish what loaded data belongs to what task
    this.loadData = function(idList, level, filter, taskRef){
      sockets.records.send({"level":level, "ids": idList, "filter": filter.filter, "taskRef": taskRef});
    };


    // loads all topojson files from the given idList array and administrationlevel
    // after everything is loaded it calls the fileLoaded function
    // input: tileList -> array of correctly formatted tile_ids --> long_lat
    this.loadGeo = function(tileList, level){

      // create the filenames of every id in tileList and load it from the server
      if(level === 0){
        queue()
          .defer(d3.json, choro.settings.datafolder + "adm0.json")
          .await(fileLoaded);

      }else{
        for (var i = 0; i < tileList.length; i++) {
          queue()
            .defer(d3.json, choro.settings.datafolder + "adm" + level + "/adm" + level + "_" + tileList[i] + ".json")
            .await(fileLoaded);
        }
      }
    };

    // loads initial tile infos from the file
    // initial tileinfos are for example what files havent got any data in them
    this.loadInitTileInfo = function(){
      queue()
        .defer(d3.json, choro.settings.datafolder + "initTiles.json")
        .await(initFileLoaded);
    };

    // show an error if an error occured
    // call the appropriate callback
    function fileLoaded(error, file){
      if(error){
        choro.uiManager.showError("Failed to load map files.");
        return;
      }

      choro.dataManager.mapGeoValues(file);
    }

    // show an error if an error occured
    // call the appropriate callback
    function initFileLoaded(error, file){
      if(error){
         choro.uiManager.showError("Failed to load init file.");
         return;
      }

      choro.tileManager.initTiles(file);
    }

    this.startSocket = function(name, server, callback){
      sockets[name] = new SocketConnection(name, server, callback);
    };

    // initable Socket object
    // messages that are recieved with this function must be stringified json elements (eg. data = '{"json": 1}' )
    // messages are automatically converted from string to json and vice-versa
    // establishes Websocket Connection and maps the neccessary functions to it
    // sends objects with the send function using the websocket
      // data must be an json object
        // stringification of the objects is done in this function
      // if websocket is not ready yet it waits a short time and tries again
    var SocketConnection = function(name, server, callBack){
      // to use "this" object in subfunctions
      var self = this;

      this.active = false;
      this.server = server;
      this.callBack = callBack;
      this.name = name;

      // establish the connection
      var connection = new WebSocket(server);

      // handler thats get called after connection is established
      connection.onopen = function(){
        self.active = true;
      };

      // Handler that gets called when a new message was recieved
      // parses the data and sends it to the callback function
      connection.onmessage = function(event){
        var data = JSON.parse(event.data);
        if(data.error === true){
          choro.uiManager.showError(error.message);
        }else{
          self.callBack(data);
        }
      };

      // if the socket connection closes (this event should only happen if the server is momentarily not available)
      // the application shows an error and retries to establish a connection after a given interval
      connection.onclose = function(){
        self.active = false;
        choro.uiManager.showError("Lost connection to server. Trying to reconnect in a moment.");
        setTimeout(function(){choro.connectionManager.startSocket(self.name, self.server, self.callBack);}, choro.settings.data.websocketReconnection);
      };

      // sends the data stringified to the websocket
      // if the socket is not yet ready it waits a short time and tries again
      this.send = function(data){
        if(this.active){
          //send the data with the connection
          connection.send(JSON.stringify(data));
        }else{
          // else try later
          window.setTimeout(function(){
            self.send(data);
          }, 2000);
        }
      };
    };
  }

  // encapsulates all functions regarding user interface, initializes the interface, handles user interaction with the interface
  // accesses settings, zoom, d3, displayManager
  // visible to public: init(), showError(error), setMeta(meta), startLoading(), stopLoading(), getCopubID(), displayInfo(), hideInfo(), updateScale(scale), initSM(data), getFilter()
  function UIManager(){
    var infoBox, infoBoxMessage, infoCopubBTN;
    var loadingView;
    var errorView, errorMessage;
    var loadingCount = 0;
    var scaleView;
    var mapView;

    var filterManager;

    // initializes the UI
    this.init = function(){
      loadingView = d3.select("#loading");

      infoBox = d3.select("#infoBox");
      infoBoxMessage = infoBox.select("#infoBoxInfo");
      infoCopubBTN = infoBox.select("#copubBTN");

      errorView = d3.select("#errorView");
      errorMessage = errorView.select("#errorMessage");

      mapView = d3.select("#map");

      scaleView = document.getElementById("scale");

      d3.select("#scaleMin").attr("value", choro.settings.data.scaleMin).on("change", scaleMinChanged);
      d3.select("#scaleMax").attr("value", choro.settings.data.scaleMax).on("change", scaleMaxChanged);

      //bind ui controls
      d3.select("#copubBTN").on("click", toggleCopub);

      d3.select("#zoomIn").on("click", function(){zoomClick(1);});
      d3.select("#zoomOut").on("click", function(){zoomClick(-1);});
      d3.select("#navUp").on("click", function(){navClick(1,0);});
      d3.select("#navDown").on("click", function(){navClick(-1,0);});
      d3.select("#navLeft").on("click", function(){navClick(0,1);});
      d3.select("#navRight").on("click", function(){navClick(0,-1);});

      d3.selectAll(".menuBTN").on("click", toggleOverlay);
      d3.selectAll(".closeOverlayBTN").on("click", closeOverlay);

      //initialize state buttons
      d3.selectAll(".stateBTN").each(function(){
        var callBack;
        var initState;
        switch(this.id){
          case "persistScale":
            callBack = changeScalePersistence;
            initState = (choro.settings.data.updateScale) ? 0 : 1;
          break;
          case "scaleType":
            callBack = changeScaleType;
            initState = choro.settings.data.scaleType;
          break;
          case "adminChange":
            callBack = changeAdminLevelStop;
            initState = (choro.settings.map.stopLevelChanges) ? 1 : 0;
          break;
          case "colorScaleChange":
            callBack = changeColorScale;
            initState = choro.settings.data.colorScale;
          break;
        }

        var s = new StateButton(this, callBack);

        if(initState !== undefined){
          s.setState(initState);
        }
      });

      filterManager = new FilterManager();
      filterManager.init();

      bindKeys();
    };

    this.showError = function(error){
      errorMessage.node().innerHTML = "Error: <br />" + error;
      errorView.style("display", "block");
    };

    function changeColorScale(state){
      choro.settings.data.colorScale = state;
      choro.displayManager.changeColorScale();
    }

    function bindKeys(){
      //bind keys
      document.onkeydown = function(e){
        e = e || window.event;
        switch(e.which || e.keyCode){
          case 38:// up Key
          case 87:// w Key
            navClick(1,0);
          break;

          case 40:// down Key
          case 83:// s key
            navClick(-1,0);
          break;

          case 37:// left Key
          case 65:// a key
            navClick(0,1);
          break;

          case 39:// right Key
          case 68:// d key
            navClick(0,-1);
          break;

          case 187:// +
            zoomClick(1);
          break;

          case 189:// -
            zoomClick(-1);
          break;

          case 70:// f key
            var evt = document.createEvent("MouseEvents");
            evt.initMouseEvent('click');
            document.getElementById("filterBTN").dispatchEvent(evt);
          break;
        }
      };
    }

    function changeScalePersistence(state){
      choro.settings.data.updateScale = ((state === 0) ? true : false);
    }
    function changeScaleType(state){
      choro.settings.data.scaleType = state;
    }
    function changeAdminLevelStop(state){
      choro.settings.map.stopLevelChanges = ((state === 0) ? false : true);
    }

    function scaleMinChanged(){
      choro.settings.data.scaleMin = this.value;
      choro.dataManager.setScales(choro.settings.data.scaleMin, choro.settings.data.scaleMax);
    }
    function scaleMaxChanged(){
      choro.settings.data.scaleMax = this.value;
      choro.dataManager.setScales(choro.settings.data.scaleMin, choro.settings.data.scaleMax);
    }

    // sets various metadata about the current state of the map
    // eg filter
    this.setMeta = function(metadata){
        mapView.attr(metadata);
    };

    // starts showing a View that indicates loading
    this.startLoading = function(){
      loadingCount++;
      loadingView.style("display", "block");
    };
    // stops showing the View that indicates loading
    this.stopLoading = function(){
      if(--loadingCount <= 0){
        loadingView.style("display", "none");
      }
    };

    // displays infobox with information about the clicked country
    // if it is turned of in the settings, this will do nothing
    // infoboxes are also used to display the copub button
    this.displayInfo = function(){
      // stop if this feature is disabled in the settings or another function has claimed this event (eg. pan map)
      if(choro.settings.ui.info === false || d3.event.defaultPrevented){return;}

      var x = d3.event.x || d3.event.pageX;
      var y = d3.event.y || d3.event.pageY;

      infoBox
        .style("display", "inline-block")
        .style("left", x + "px")
        .style("top", y + "px");

      var el = d3.select(this);
      var level = el[0][0].parentNode.getAttribute("data-level");
      var id = el.attr("data-id");
      var filterIndex = mapView.attr("data-filterIndex");

      var text = "<strong>" + el.attr("data-name") + "</strong><br />" +
          choro.dataManager.getRecordInfo(level, id, filterIndex).toLocaleString("de") + " Records<br />";

      if(choro.settings.debug.debugMode){
        text += "<br />ID: " + el.attr("data-id");
      }
      infoBox.attr("data-id", id);
      infoBoxMessage.node().innerHTML = text;

      // if clicked on country that has copub enabled --> it displays hide copublication, else it displays show copublications
      if(filterManager.getCopubID() === id){
        infoCopubBTN.node().innerText = "hide copublications";
      }else{
        infoCopubBTN.node().innerText = "show copublications";
      }
    };

    // hides infoBox
    this.hideInfo = function(){
      infoBox.style("display", "none");
    };

    this.getCopubID = function(){
      return filterManager.getCopubID();
    };

    function toggleCopub(){
      var id = infoBox.attr("data-id");

      filterManager.setCopubID(infoBox.attr("data-id"));

      choro.uiManager.hideInfo();
    }

    this.updateScale = function(scale){
      // do nothing if the scale is not ready yet
      if(scale.invert(1) === scale.invert(8) || isNaN(scale.invert(1))){return;}

      var labels = scaleView.getElementsByTagName("label");
      for (var i = 0; i < 9; i++) {
        labels[i].innerHTML = "< " + Math.ceil(scale.invert(i+1)).toLocaleString("de");
      }
      labels[8].innerHTML = ">= " + Math.ceil(scale.invert(8)).toLocaleString("de");
    };

    this.initSM = function(data){
      filterManager.initSM(data);
    };
    this.getFilter = function(){
      return filterManager.getFilter();
    };

    function closeOverlay(){
      this.parentNode.parentNode.style.display="none";
    }

    function toggleOverlay(){
      var overlay = d3.select("#" + this.getAttribute("data-overlay"))[0][0];

      //calculate the new state it gets
      var newState = (overlay.style.display == "block" ? "none" : "block");

      // remove all overlays
      d3.selectAll(".overlay").style("display","none");

      overlay.style.display = newState;
    }

    //zooms the map by using the ui control buttons
    function zoomClick(direction){ //http://bl.ocks.org/mgold/c2cc7242c8f800c736c4
      if(d3.event !== null){
        d3.event.preventDefault();
        d3.event.stopPropagation();
      }

      var factor = (direction === 1) ? choro.settings.ui.zoomfactor[0] : choro.settings.ui.zoomfactor[1],
          extent = zoom.scaleExtent(),
          translate = zoom.translate(), x = translate[0], y = translate[1],
          newScale = zoom.scale() * factor;

      //if max or min zoom was reached dont do anything
      if(newScale <= extent[0] || newScale >= extent[1]){return false;}

      //calculate new center for translation
      x = (x - center[0]) * factor + center[0];
      y = (y - center[1]) * factor + center[1];

      //zoom
      zoom.scale(newScale)
        .translate([x,y]);

      choro.displayManager.zoomMapEnd();
    }

    //pans the map using the ui control buttons
    function navClick(vertical, horizontal){
      if(d3.event !== null){
        d3.event.preventDefault();
        d3.event.stopPropagation();
      }

      var factor = choro.settings.ui.panfactor;

      zoom.translate([zoom.translate()[0] + horizontal * factor, zoom.translate()[1] + vertical * factor]);
      choro.displayManager.zoomMapEnd();
    }

    // initializes and handles interaction with the filter
    var FilterManager = function(){
      var Manager;
      var filterView;
      var currentFilter = {};
      var currentFilterIndex = -1;
      var lastFilterIndex = -1;
      var copubID = -1;

      // saves all filters with filterID
      // if filter did exist previously in this session --> it allows a reuse of queried data
      var filterList = [];

      this.init = function(){
        Manager = this;
        filterView = d3.select("#filter");

        initBounds();

        d3.selectAll(".groupTitle").on("click", toggleSubFilter);
        d3.select(".resetBTN").on("click", resetAllFilter);
        d3.selectAll(".submitBTN").on("click", this.filter);

        d3.selectAll(".rangeFilter").selectAll("input").on("change", rangeChange);
      };

      function initBounds(){
        //init year
        if(choro.settings.filter.yearmax === -1){// if -1 set end to current year
          choro.settings.filter.yearmax = new Date().getFullYear();
        }

        var year = d3.select("#filterYear");

        year.select(".fromRange").selectAll("input")
          .attr("min", choro.settings.filter.yearmin)
          .attr("max", choro.settings.filter.yearmax)
          .attr("value", choro.settings.filter.yearmin);

        year.select(".toRange").selectAll("input")
          .attr("min", choro.settings.filter.yearmin)
          .attr("max", choro.settings.filter.yearmax)
          .attr("value", choro.settings.filter.yearmax);

        //init citations
        var citations = d3.select("#filterTimesCited");

        citations.select(".fromRange").selectAll("input")
          .attr("min", choro.settings.filter.citationMin)
          .attr("max", choro.settings.filter.citationMax)
          .attr("value", choro.settings.filter.citationMin);

        citations.select(".toRange").selectAll("input")
          .attr("min", choro.settings.filter.citationMin)
          .attr("max", choro.settings.filter.citationMax)
          .attr("value", choro.settings.filter.citationMax);

        //init author
        var filterAuthor = d3.select("#filterAuthorCount");

        filterAuthor.select(".fromRange").selectAll("input")
          .attr("min", choro.settings.filter.authorMin)
          .attr("max", choro.settings.filter.authorMax)
          .attr("value", choro.settings.filter.authorMin);

        filterAuthor.select(".toRange").selectAll("input")
          .attr("min", choro.settings.filter.authorMin)
          .attr("max", choro.settings.filter.authorMax)
          .attr("value", choro.settings.filter.authorMax);

        //init involved countries
        var filterInvolved = d3.select("#filterInvolvedCountries");

        filterInvolved.select(".fromRange").selectAll("input")
          .attr("min", choro.settings.filter.involvedCountriesMin)
          .attr("max", choro.settings.filter.involvedCountriesMax)
          .attr("value", choro.settings.filter.involvedCountriesMin);

        filterInvolved.select(".toRange").selectAll("input")
          .attr("min", choro.settings.filter.involvedCountriesMin)
          .attr("max", choro.settings.filter.involvedCountriesMax)
          .attr("value", choro.settings.filter.involvedCountriesMax);
      }

      this.initSM = function(data){
        var smList = document.getElementById("filterSm").getElementsByClassName("groupContent")[0];
        var template = document.getElementById("smTemplate");

        for(var i = 0; i < data.length; i++){
          var id = "filterSM1_" + data[i].id1;

          var level1 = document.getElementById(id);

          if(level1 === null){
            level1 = template.cloneNode(true);
            level1.id=id;
            level1.getElementsByClassName("title")[0].innerHTML = data[i].name1;
            level1.getElementsByClassName("groupTitle")[0].onclick = toggleSubFilter;
            level1.getElementsByClassName("contentCheck")[0].onclick = smSubCheck;
            level1.getElementsByClassName("contentCheck")[0].setAttribute("data-level", 1);

            smList.appendChild(level1);
          }

          var level2 = template.cloneNode(true);
          level2.id="filterSM2_" + data[i].id2;
          level2.getElementsByClassName("title")[0].innerHTML = data[i].name2;
          level2.getElementsByClassName("groupTitle")[0].onclick = toggleSubFilter;
          level2.getElementsByClassName("contentCheck")[0].onclick = smSubCheck;
          level2.getElementsByClassName("contentCheck")[0].setAttribute("data-level", 2);

          var content1 = level1.getElementsByClassName("groupContent")[0];
          content1.appendChild(level2);

          var content2 = level2.getElementsByClassName("groupContent")[0];
          for(var j = 0; j < data[i].name3list.length; j++){
            var namePair = data[i].name3list[j].split(",");

            var idSM = "sm_" + namePair[0];

            var label = document.createElement("label");
            label.innerHTML = namePair[1];
            label.for = idSM;

            var checkbox = document.createElement("input");
            checkbox.type = "checkbox";
            checkbox.checked = true;
            checkbox.id = idSM;
            checkbox.name = idSM;
            checkbox.className += "smCheck";
            checkbox.setAttribute("data-id", namePair[0]);
            checkbox.onclick = smSubCheck;

            content2.appendChild(checkbox);
            content2.appendChild(label);
            content2.appendChild(document.createElement("br"));
          }
        }
      };

      // checks / unchecks other levels of sm
      // depends on that the dom structure hasnt changed within #filterSm
      function smSubCheck(event){
        // prevent event to propagation
        if(event.stopPropagation){
          event.stopPropagation();
        }else{
          window.event.cancelBubble = true; // for ie
        }

        var level = parseInt(this.getAttribute("data-level"));

        if(level===1){
          // check and uncheck lower levels
          var state1 = this.childNodes[0].checked;
          var parent1 = this.parentNode.parentNode;
          var boxes1 = parent1.getElementsByTagName("input");

          //check all lower level checkboxes to the upper level state and set every indeterminate checkbox to false
          for (var i = 0; i < boxes1.length; i++) {
            boxes1[i].indeterminate = false;
            boxes1[i].checked = state1;
          }
        }else if(level===2){
          // check and uncheck lower level
          var state2 = this.childNodes[0].checked;
          var parent2 = this.parentNode.parentNode;
          var boxes2 = parent2.getElementsByTagName("input");

          //check all lower level checkboxes to the upper level state
          for (var j = 0; j < boxes2.length; j++) {
            boxes2[j].checked = state2;
          }

           // set upper level to true/false/indeterminate according to the checkboxes of this level
          var level1 = this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("contentCheck")[0].childNodes[0];
          var level2CheckBoxes = this.parentNode.parentNode.parentNode.getElementsByTagName("input");

          var indeterminate = false;

          // check if any sibling checkboxes has another state
          for (var k = 0; k < level2CheckBoxes.length; k++) {
            if(level2CheckBoxes[k].checked !== state2 || level2CheckBoxes[k].indeterminate===true){
              indeterminate = true;
              break;
            }
          }

          //set indeterminate and/or checked state accordingly
          if(indeterminate){
            level1.indeterminate = true;
          }else{
            level1.indeterminate = false;
            level1.checked = state2;
          }

        }else{ // level == 3
          var state3 = this.checked;

          // check if all values of this level are the same
          var boxes3 = this.parentNode.getElementsByTagName("input");

          var indeterminate3 = false;

          for (var l = 0; l < boxes3.length; l++) {
            if(boxes3[l].checked !== state3 || boxes3[l].indeterminate===true){
              //set upper level to indeterminate
              indeterminate3 = true;
              break;
            }
          }

          var level1 = this.parentNode.parentNode.parentNode.parentNode.getElementsByClassName("contentCheck")[0].childNodes[0];
          var level2 = this.parentNode.parentNode.getElementsByClassName("contentCheck")[0].childNodes[0];

          if(indeterminate3){
           level1.indeterminate = true;
           level2.indeterminate = true;
          }else{
            level2.checked = state3;
            level2.indeterminate = false;

            //check on level 2 if either checkbox is indeterminate or has another state
            var level2CheckBoxes3 = this.parentNode.parentNode.parentNode.getElementsByTagName("input");
            var level1Indeterminate1 = false;

            for (var m = 0; m < level2CheckBoxes3.length; m++) {
              if(level2CheckBoxes3[m].checked !== state3 || level2CheckBoxes3[m].indeterminate === true){
                level1Indeterminate1 = true;
                break;
              }
            }

            if(level1Indeterminate1){
              level1.indeterminate = true;
            }else{
              level1.indeterminate = false;
              level1.checked = state3;
            }
          }
        }
      }

      // checks if the currentFilter is in the filterList
      // returns the filter with an index
        // index is lastFilterIndex + 1 if filter wasnt in filterList
        // else index is the index of the filter in the filterList
      // appends filter in the filterList, so that the returned index is the index of this filter
      this.getFilter = function(){
        // if the filter was already found
        if(currentFilterIndex >= 0){
          return {"index": currentFilterIndex, "filter": currentFilter};
        }

        var stringFilter = JSON.stringify(currentFilter);
        var index = filterList.indexOf(stringFilter);

        if(index === -1){
          filterList[++lastFilterIndex] = stringFilter;
          index = lastFilterIndex;
        }

        currentFilterIndex = index;

        return {"index": index, "filter": currentFilter};
      };

      this.filter = function(){
        filter = {};

        // sm
        var unchecked = false;
        var smList = [];
        filterView.selectAll(".smCheck").each(function(){
          if(this.checked){
            smList.push(this.getAttribute("data-id"));
          }else{
            unchecked = true;
          }
        });

        //only append the list if any checkbox was unchecked
        if(unchecked){
          filter.sm = smList;
        }

        //rangefilter
        filterView.selectAll(".rangeFilter").each(function(){
          if(this.getAttribute("data-fromfiltered") === "true" || this.getAttribute("data-tofiltered") === "true"){
            var range = d3.select(this);
            var start = parseInt(range.select(".fromRange").select(".range")[0][0].value);
            var end = parseInt(range.select(".toRange").select(".range")[0][0].value);

            switch(this.parentNode.id){
              case "filterYear":
                filter.year = {"start":start, "end":end};
              break;
              case "filterTimesCited":
                filter.timesCited = {"start":start, "end":end};
              break;
              case "filterAuthorCount":
                filter.authorCount = {"start":start, "end":end};
              break;
              case "filterInvolvedCountries":
                filter.involvedCountries = {"start":start, "end":end};
              break;
            }
          }
        });

        //tristate buttons
        filterView.selectAll(".tristateBTN").each(function(){
          var state = parseInt(this.getAttribute("data-state"));
          if(state !== 0){
            switch(this.name){
              case "id_scopus":
              filter.id_scopus = (state === 1);
              break;
              case "id_wos":
              filter.id_wos = (state === 1);
              break;
              case "citable":
              filter.citable = (state === 1);
              break;
            }
          }
        });

        if(copubID !== -1){
          filter.copub = copubID;
        }

        currentFilter = filter;

        // set currentFilterIndex to -1 so that the getFilter Method knows that the filter was changed
        currentFilterIndex = -1;

        // send checktiles signal
        choro.tileManager.checkTiles();
      };

      this.setCopubID = function(id){
          if(id === copubID){
            copubID = -1;
          }else{
            copubID = id;
          }

          this.filter();
      };

      this.getCopubID = function(){
        return copubID;
      };

      // resets all filter in the filtersettings
      // copub is not beeing reset.
      function resetAllFilter(){

        //reset sm
        filterView.select("#filterSm").selectAll("input").each(function(){
          this.checked = true;
          this.indeterminate = false;
        });

        //reset all range filter
        filterView.selectAll(".rangeFilter").each(function(){
          // reset filtered flags
          this.setAttribute("data-fromfiltered", false);
          this.setAttribute("data-tofiltered", false);

          // reset from range
          var fromRange = this.children[0].getElementsByClassName("range")[0];
          fromRange.value = fromRange.min;
          this.children[0].getElementsByClassName("text")[0].value = fromRange.min;

          // reset to range
          var toRange = this.children[1].getElementsByClassName("range")[0];
          toRange.value = toRange.max;
          this.children[1].getElementsByClassName("text")[0].value = toRange.max;
        });

        // reset all tristate buttons
        filterView.selectAll(".tristateBTN").each(function(){
          this.setAttribute("data-state", 0);
          this.textContent = JSON.parse(this.getAttribute("data-statetext"))[0];
        });
      }

      // method that gets called if a range or number input from a .rangefilter is changed
      // changes the associated range or number to the value that is changed
      // clips the value that it is still within the min and max bounds
      // if the from range value is higher than the to range value to and from get the same value (other way around respectively)
      // if the value changed from the initial position a changed value flag is raised on the top element (where the class .rangeFilter ist)
      // all rangeFilter Elements, where this callback is called on, must be structured equaly
      function rangeChange(){
        var parent = d3.select(this.parentNode);
        var rangeFilter = parent.node().parentNode;
        var isFrom = parent.classed("fromRange");

        // parse the input or else you compare strings which doesnt result the correct value --> ("70" < "500") === false
        var newValue = parseInt(this.value);
        var max = parseInt(this.max);
        var min = parseInt(this.min);

        // change value of text/range to given value
        if(this.type == "number"){
          // clip number to the bounds
          if(newValue < min){this.value = min;}
          if(newValue > max){this.value = max;}

          //text changed --> change range
          parent.select(".range")[0][0].value = newValue;
        }else{
          //range changed --> change text
          parent.select(".text")[0][0].value = newValue;
        }

        if(isFrom){
          var toRange = d3.select(rangeFilter).select(".toRange");

          // value must be parsed as Integer from input or else it is, for some unknown reason, a string...
          var toValue = parseInt(toRange.select(".range")[0][0].value);

          // change value of toRange Inputs if toRange is less than fromRange
          if(toValue < newValue){
            toRange.select(".range")[0][0].value = newValue;
            toRange.select(".text")[0][0].value = newValue;
          }

          // check if value is at bounds
          // set a flags on top element accordingly
          rangeFilter.setAttribute("data-fromfiltered", (newValue !== min));
          // if slider moves the second slider to the max --> set max unfiltered to true
          if(newValue === max){
            rangeFilter.setAttribute("data-tofiltered", false);
          }
        }else{
          var fromRange = d3.select(rangeFilter).select(".fromRange");

           // value must be parsed as Integer from input or else it is, for some unknown reason, a string...
          var fromValue = parseInt(fromRange.select(".range")[0][0].value);

          //change value of fromRange Inputs if fromRange is greater than toRange
          if(fromValue > newValue){
            fromRange.select(".range")[0][0].value = newValue;
            fromRange.select(".text")[0][0].value = newValue;
          }

          // check if value is at bounds
          // set a flags on top element accordingly
          rangeFilter.setAttribute("data-tofiltered", (newValue !== max));
          // if slider moves the second slider to the min --> set min unfiltered to true
          if(newValue === min){
            rangeFilter.setAttribute("data-fromfiltered", false);
          }
        }
      }

      function toggleSubFilter(){
        toggle(d3.select(this.parentNode).select(".groupContent")[0][0]);
      }
      function toggle(element){
        element.style.display = (element.style.display == "block" ? "none" : "block");
      }
    };

    // handles behaviour of "StateButtons"
    var StateButton = function(button, callBack){
      var state = button.getAttribute("data-state");
      var content = JSON.parse(button.getAttribute("data-stateText"));

      button.onclick = changeState;

      function changeState(){
        if(++state >= content.length){
          state=0;
        }
        this.textContent = content[state];
        this.setAttribute("data-state", state);

        if(callBack !== undefined){
          callBack(state);
        }
      }

      this.setState = function(s){
        state = s;
        button.textContent = content[state];
        button.setAttribute("data-state", state);
      };
    };
  }

  // encapsulates all functions regarding the managing of data.
  // data means in this case specifcally the geoData collected from the topoJson files and the publication/recordData collected from the server
  // it is the center piece of the application.
  // it gets the viewed ids from the tileManager, collects data from the connectionmanager, calls the displayManager if it should display something new
  // accesses: displayManager, connectionManager, d3,
  // visible to public: init(), display(), mapGeoValues(), mapRecordValues(), getCachesDebug(), getRecordInfo(), setScales()
  function DataManager(){
    var dataCache = {};

    // a queue that saves information about data that has yet to be loaded
    var taskQueue;

    var scaleChanged = false;

    var scales = [
      d3.scale.log() //ln scale
        .base(Math.E)
        .clamp(true)
        .range([0, 9])
        .nice(),

      d3.scale.linear() //log scale
        .clamp(true)
        .range([0, 9])
        .nice()
    ];

    // initializes the dataCache of the datamanager
    this.init = function(){
      taskQueue = new choro.TaskQueue();
      dataCache.adm0 = {"level": 0, "recData": d3.map(), "geoData": null};
      dataCache.adm1 = {"level": 1, "recData": d3.map(), "geoData": d3.map()};
      dataCache.adm2 = {"level": 2, "recData": d3.map(), "geoData": d3.map()};
      dataCache.adm3 = {"level": 3, "recData": d3.map(), "geoData": d3.map()};

      choro.dataManager.setScales(choro.settings.data.scaleMin, choro.settings.data.scaleMax);
    };

    // debug method to display the current datacache
    this.getCachesDebug = function(){
      if(choro.settings.debug.debugMode){
        return dataCache;
      }
    };

    // get the value of the Record from the id on level level and with filterIndex filterIndex
    this.getRecordInfo = function(level, id, filterIndex){
      return dataCache["adm"+level].recData.get(createKey(id, filterIndex));
    };

    // check which ids must be loaded to display.
    // if nothing must be loaded it calls the displayManager.displayMap method.
    this.display = function(tileTask){
      var ids = getUniqueIdArray(tileTask.tiles);

      var storedFilterIndex = getStoredFilterIndex(tileTask.filter);

      var loadList = getNotLoadedValues(ids.rec, ids.geo, tileTask.level, storedFilterIndex);

      var task = {"visibleIds": ids.rec, "level": tileTask.level, "storedFilterIndex": storedFilterIndex};

      // load geoData if neccessary
      if(loadList.geo.length !== 0){
        choro.connectionManager.loadGeo(loadList.geo, tileTask.level);
      }

      // load records if neccessary
      if(loadList.rec.length !== 0){
        choro.uiManager.startLoading();// increase loadingCount by one because it now will decreased by two (once normal and once after records where loaded)
        var taskRef = taskQueue.setTask(task);
        choro.connectionManager.loadData(loadList.rec, tileTask.level, tileTask.filter, taskRef);
      }
      colorize(task);
    };

    // callback method of the geoconnection
    // maps the given data to the appropriate tile
    this.mapGeoValues = function(data){
      if(data.level === 0){
        dataCache.adm0.geoData = data;
      }else{
        dataCache["adm" + data.level].geoData.set((+data.long + 180) + "_" + (+data.lat + 90), data);
      }

      choro.displayManager.drawShapes(data, data.level);
    };

    // callback method of the record Socket
    // maps the given data to the appropriate datacache location
    // calls displayQueuedELement if ready
    this.mapRecordValues = function(data){
      if(data.init){
        choro.uiManager.initSM(data.sm);
      }else{
        var task = taskQueue.getTask(data.taskRef);

        if(task === -1){
          console.log("Error: couldnt find task ref:" + data.taskRef);
          return;
        }

        for (var i = 0; i < data.recData.length; i++) {
          dataCache["adm" + data.level].recData.set(createKey(data.recData[i].id, task.storedFilterIndex), data.recData[i].n);

          //stores data in the localstorage
          localStoreData(data.recData[i].id, task.storedFilterIndex, data.recData[i].n);
        }
        colorize(task);
      }
    };

    // gets the correctData for displaying and displays them.
    // adds all new ids to the displayed array
    function colorize(task){
      var data = getDisplayData(task);
      choro.uiManager.setMeta({"data-filterIndex": task.storedFilterIndex});
      choro.displayManager.colorizeMap(data, scaleChanged);
      scaleChanged = false;
    }

    // checks which ids arent loaded yet.
    // input: array of ids, array of tileIDs, the datacache with the correct administration level eg dataCache["adm0"]
    // returns: object with array of ids that need to be loaded for recorddata and an array that needs to be loaded for geodata
    function getNotLoadedValues(ids, tileIDs, level, storedFilterIndex){
      var loadList = {"rec": [], "geo": []};

      //get the current cache
      var cache = dataCache["adm"+level];

      // get geodata to load
      if(cache.geoData === null){
        // load worldmap
        loadList.geo.push(0);
      }else if(cache.geoData.type === undefined){
        // is an d3.map() object --> admin level 1 or higher
        // get tiles that need to be loaded
        tileIDs = tileIDs.unique();
        for (var i = 0; i < tileIDs.length; i++) {
          if(!cache.geoData.has(tileIDs[i])){
            loadList.geo.push(tileIDs[i]);
          }
        }
      }

       // get record data to load
      for (var k = 0; k < ids.length; k++) {
        // check if value is already in the recordmap
        if(!cache.recData.has(createKey(ids[k], storedFilterIndex))){
          // check if value is in localstorage
          var v = getLocalStoredData(ids[k], storedFilterIndex);
          if(v !== false){
            //if it is, set value in the map
            cache.recData.set(createKey(ids[k], storedFilterIndex), v);
          }else{
            //if it isnt, load it from the server
            loadList.rec.push(ids[k]);
            // and set value to -1 to prevent duplicated load requests of the same id
            cache.recData.set(createKey(ids[k], storedFilterIndex), -1);
          }
        }
      }

      return loadList;
    }

    // creates an object with a list of unique ids and a unique list of geoTileNames to load
    // created object contains only ids and geoTileNames that are within the given tiles
    function getUniqueIdArray(tiles){
      var idArray = [];
      for (var i = 0; i < tiles.length; i++) {
        idArray = idArray.concat(tiles[i].contents);
      }
      idArray = idArray.unique();

      var idGeoArray = [];
      for (var j = 0; j < tiles.length; j++) {
        idGeoArray = idGeoArray.concat(tiles[j].geoTiles);
      }
      idGeoArray = idGeoArray.unique();

      return {"rec": idArray, "geo": idGeoArray};
    }

    function createKey(id, filterIndex){
      return "f" + filterIndex + "id" + id;
    }

    function updateScales(ids, cache, filter){
      if(!choro.settings.data.updateScale){return;}

      var max = 0;
      var min = Infinity;

      // get max and minimum from the currently viewed ids
      for (var i = 0; i < ids.length; i++) {
        var value = cache.recData.get(createKey(ids[i], filter));
        if(value < min){
          min = value;
        }
        if(value > max){
          max = value;
        }
      }

      choro.dataManager.setScales(min, max);
    }

    // sets the domain of every scale and calls the uimanager to indicate that their scale also must be updated
    // changes scaleChanged var if the scale changed
    this.setScales = function(min, max){
      for (var i = scales.length - 1; i >= 0; i--) {
        var before = scales[i].domain();
        scales[i].domain([Math.max(min, 0.1), max]);
        var after = scales[i].domain();

        if(before[0] !== after[0] || before[1] !== after[1]){
          scaleChanged = true;
        }
      }
      choro.uiManager.updateScale(scales[choro.settings.data.scaleType]);
    };

    // gets all records from the given ids
    // this creates the elements in the correct form that is used by the displaymanager
    function getDisplayData(task){
      var ids = task.visibleIds;
      var cache = dataCache["adm"+task.level];
      var filter = task.storedFilterIndex;

      var scale = scales[choro.settings.data.scaleType];

      updateScales(ids, cache, filter);

      // create new data element with pairs of ids and scaled values
      var data = [];
      for (var i = 0; i < ids.length; i++) {
        var value = cache.recData.get(createKey(ids[i], filter));
        if(value !== -1){
          data.push({"id": ids[i], "value": scale(value)});
        }
      }

      return data;
    }

    // stores the data in localstorage, if its enabled
    // adds "created" value to check at a later time, if new data has to be loaded or the cached data can be used
    // uses filterIndex to store values depending on the storedFilterIndex
    function localStoreData(key, storedFilterIndex, value){
      if(choro.settings.data.useLocalstorage){
        var v = {"value": value, "created": choro.settings.data.startTime};
        localStorage.setItem("f"+storedFilterIndex+"id"+key, JSON.stringify(v));
      }
    }

    // if localstorage is enabled, it searches for the value in localstorage
    // uses filterIndex to store values depending on the storedFilterIndex
    // method checks if the value is in localstorage and not yet expired
    // returns value if found and not expired
    // returns false if localstorage not enabled, value not found or value expired
    function getLocalStoredData(key, storedFilterIndex){
      if(choro.settings.data.useLocalstorage){
        var v = JSON.parse(localStorage.getItem(createKey(key, storedFilterIndex)));
        if(v === null || v.created + choro.settings.data.timeToExpire < choro.settings.data.startTime){
          return false;
        }else{
          return v.value;
        }
      }else{
        return false;
      }
    }

    // searches localstorage for the filter
      // adds the filter/creates the filterList if the filter or the filterList doesnt exist.
    // returns the StorageIndex of the filter
    // returns the filter.index (from input argument) if localstorage isnt enabled
    function getStoredFilterIndex(filter){
       if(choro.settings.data.useLocalstorage){
        var filterList;

        // get the item from localstorage
        try{
          filterList = JSON.parse(localStorage.getItem("filterList"));
        }catch(SyntaxError){
          // JSON.parse returns an SyntaxError if it couldnt parse it --> shouldnt happen normally but catch it anyway
          console.log("Error: Couldnt parse localStorage filterlist");
        }

        var filterString = JSON.stringify(filter.filter);

        // if it is not in localstorage (var is null) or there was an SyntaxError (var is undefined)
        if(filterList === undefined || filterList === null){
          // set the given filter as the first item in the filterList and return 0
          localStorage.setItem("filterList", JSON.stringify([filterString]));
          return 0;
        }

        var index = filterList.indexOf(filterString);

        // if the filter doesnt exist, add it to the Storage and return its index
        if(index === -1){
          index = filterList.push(filterString);
          localStorage.setItem("filterList", JSON.stringify(filterList));
          // return the index given from the push method minus one (cause push returns index + 1)
          return index - 1;
        }

        return index;
      }
      return filter.index;
    }
  }

  // encapsulates all functions that are used to create and utilize tiles, so that other objects know which shapes and data to display
  // visible to public: init(), initTiles(), getBoundingBox(), screenToGeoCoords(), geoToScreenCoords(), getTiles(), checkTiles(), cacheTiles()
  function TileManager(){
    // holds all tiles
    var tiles = [];
    // holds the currently displayed tiles
    var displayedTiles = null;

    // currentAdm holds the current admin level to display the correct admin level on the map
    var currentAdm = 0;

    var currentFilterIndex = -1;

    taskQueue = new choro.TaskQueue();

    // initializes the tiles and checks what to display initially
    this.init = function(){
      tiles = createTiles();

      choro.connectionManager.loadInitTileInfo();

      this.checkTiles();
    };

    // initializes tiles
    // data is a json file with an array of tiles coords with no data in them
    this.initTiles = function(data){
      for (var i = 0; i < data.noTiles.length; i++) {
        if(tiles[data.noTiles[i].level] === undefined || tiles[data.noTiles[i].level][data.noTiles[i].long] === undefined || tiles[data.noTiles[i].level][data.noTiles[i].long][data.noTiles[i].lat] === undefined){
          console.log("init failure");
          console.log(data.noTiles[i].level, data.noTiles[i].long, data.noTiles[i].lat);
          console.log(tiles[data.noTiles[i].level], tiles[data.noTiles[i].level][data.noTiles[i].long]);
        }
        tiles[data.noTiles[i].level][data.noTiles[i].long][data.noTiles[i].lat].loaded = true;
      }
    };

    // debug method to access all tiles
    this.tilesDebug = function(){
      if(choro.settings.debug.debugMode){
        return tiles;
      }
    };

    // gets the bounding box of the svg Element in geocoordingates(long lat) according to the current projection
    // returns the bounding rectangle
      // Bounding Box is restricted by the longitude interval [-180, 180] and the latitude interval [-90, 90]
    this.getBoundingBox = function(){
      var lt = this.screenToGeoCoords([0, 0]);
      var rb = this.screenToGeoCoords([width, height]);

      var bbox = {
        "left": restrictToBounds(lt[0], 180),
        "right": restrictToBounds(rb[0], 180),
        "top": restrictToBounds(lt[1], 90),
        "bottom": restrictToBounds(rb[1], 90),
        "level": currentAdm
      };
      return bbox;
    };

    // converts the screen coordinates to geo coordinates(long, lat)
    // first the translation and zoom level of the group element will be removed
    // than these vars will use the invert method of d3.projection to get the coordinates
    this.screenToGeoCoords = function(point){
      var geoCoords = [0, 0];

      geoCoords[0] = (point[0] - zoom.translate()[0]) / zoom.scale();
      geoCoords[1] = (point[1] - zoom.translate()[1]) / zoom.scale();

      return projection.invert(geoCoords);
    };

    // converts geo Coordinates to screen coordinates
    // first the d3.projection method is used to convert them
    // than the zoom and translate of the group element will be applied
    this.geoToScreenCoords = function(point){
      point = projection(point);

      var screenCoords = [];
      screenCoords[0] = (point[0] * zoom.scale()) + zoom.translate()[0];
      screenCoords[1] = (point[1] * zoom.scale()) + zoom.translate()[1];

      return screenCoords;
    };

    // gets the Start and EndTile Index of longitude and latitude from getTileBounds
    // returns an array of every tile within these bounds
    // depends on the Administration level of the tilebounds object
    this.getTiles = function(tileBounds){
      var outTiles = [];

      for(var i = tileBounds.long.start; i <= tileBounds.long.end; i++){
        for(var j = tileBounds.lat.start; j <= tileBounds.lat.end; j++){
          outTiles.push(tiles[tileBounds.zoom][i][j]);
        }
      }

      return outTiles;
    };

    // checks if the map accesses a new tile and loads the data if it is
    this.checkTiles = function(){
      if(choro.settings.debug.newDataStop){return;}

      if(!choro.settings.map.stopLevelChanges){
        checkZoomLevel();
      }
      var bbox = this.getBoundingBox();
      var viewedTiles = getTileBounds(bbox);

      var filter = choro.uiManager.getFilter();

      var tiles = checkDisplay(viewedTiles, filter);

      // only do something if there was a different in the current and previous displayed tiles
      if(tiles !== null){
        loadTiles(tiles);
      }
    };

    // checks if there is a difference between the current and the previous display
    // checks if the display was initialized || the filter changed || the admin level changed || the bounds changed
    // returns true if something changed, writes current state into displayedTiles
    // returns false if everything stayed the same
    function checkDisplay(newTiles, filter){
      var t;

      if(displayedTiles === null){
        // if it is the first time
        t = choro.tileManager.getTiles(newTiles);
        displayedTiles = {"bounds": newTiles, "tiles": t, "filter": filter};
        return displayedTiles;
      }else if(displayedTiles.bounds.zoom !== currentAdm){
        // if the zoom level changed
        t = choro.tileManager.getTiles(newTiles);

        displayedTiles.bounds = newTiles;
        displayedTiles.tiles = t;
        return displayedTiles;
      }else if(displayedTiles.filter.index !== filter.index){
        // if a new filter was set
        displayedTiles.filter = filter;
        return displayedTiles;
      }else if(displayedTiles.bounds.long.start !== newTiles.long.start || displayedTiles.bounds.long.end !== newTiles.long.end || displayedTiles.bounds.lat.start !== newTiles.lat.start || displayedTiles.bounds.lat.end !== newTiles.lat.end){
        // if the currently displayed tiles and the currently shown tiles are not the same
        t = choro.tileManager.getTiles(newTiles);

        displayedTiles.bounds = newTiles;
        displayedTiles.tiles = t;
        return displayedTiles;
      }

      return null;
    }

    //detects if the adminlevel changed and writes the new adminlevel to currentAdm
    function checkZoomLevel(){
      // for loop so it depends on the settings object how many zoom levels exist
      // checks every entry of the zoomlevel setting to get the current zoom level and if it is new it will write it into the currentAdm Var
      for(var i = 0; i < choro.settings.map.highestAdminLevel + 1; i++){
        if(zoom.scale() <= choro.settings.map.zoomlevels[i]){
            if(currentAdm != i){
              currentAdm = i;
              choro.displayManager.changeLevel(currentAdm);
            }
          break;
        }
      }
    }

    // callback from the connectionmanager
    // writes the loaded tiles into the tiles array and changes load flag to true
    // also writes a reference to shapetiles which needs to be loaded if this tile is beeing displayed
    // calls the display function of the datamanager, if everything is loaded
    this.cacheTiles = function(result){
      var readyTiles = [];

      for(var i = 0; i < result.tiles.length; i++){
        var tile = result.tiles[i];
        t = tiles[tile.index[0]][tile.index[1]][tile.index[2]];

        if(!t.loaded){
          t.contents = tile.data.contents;
          if(tile.data.geoTiles === null){
            t.geoTiles = [];
          }else{
            t.geoTiles = tile.data.geoTiles;
          }
          t.loaded = true;

          readyTiles.push(t);
        }
      }

      var task = taskQueue.getTask(result.taskRef);
      task.tiles = readyTiles.concat(task.tiles);

      choro.dataManager.display(task);
    };

    // creates an object with a list of tiles that need to be loaded and a list with tiles that are ready to be displayed
    // the list with the tiles for loading are minimized to the neccessary parts (to minimize the transmitted data)
    function getLoadArray(t){
      var loadArray = {"load": {"tiles": []}, "disp": []};

      for (var i = 0; i < t.length; i++) {
        if(!t[i].loaded){
          loadArray.load.tiles.push({
            "rect": t[i].rect,
            "index": t[i].index,
          });
        }else{
          loadArray.disp.push(t[i]);
        }
      }

      return loadArray;
    }

    // checks if it has to load tile contents from the server
    // if it has to than it loads it,
    // if not it calls the display function of the dataManager
    function loadTiles(dTiles){
      choro.uiManager.startLoading();

      var loadArray = getLoadArray(dTiles.tiles);

      task = {"tiles": loadArray.disp, "level": dTiles.bounds.zoom, "filter": dTiles.filter};

      if(loadArray.load.tiles.length !== 0){
        var ref = taskQueue.setTask(task);
        loadArray.load.taskRef = ref;
        choro.connectionManager.loadIds(loadArray.load);
      }
      if(loadArray.disp.length !== 0){
        choro.dataManager.display(task);
      }
    }

    // restricts a number to an interval between upper and lower bound
    // upper and lower bounds are the same value with different sign
    // value is the value that is to be restricted
    // returns the upper or lower bound if value is outside the bounds interval
    // returns the value if its inside the interval
    function restrictToBounds(value, bounds){
      return Math.max(Math.min(value, bounds), -bounds);
    }

    // calculates which tiles are within the given bounding box
    // returns an object which holds the tile array index of start- and endTile of longitude and latitude
    // depends on the given boundingBox Administration level
    var getTileBounds = function(bbox){
      var steps = choro.settings.map.tileSize[bbox.level];

      // translate the points so the bounding box holds intervals from 0-360 and 0-180
      bbox.left += 180;
      bbox.right += 180;
      bbox.bottom += 90;
      bbox.top += 90;

      //calculates the tile start and end Indices
      var longTileStart = ((bbox.left - (bbox.left % steps)) / steps);
      var longTileEnd = ((bbox.right - (bbox.right % steps)) / steps);

      var latTileStart = ((bbox.bottom - (bbox.bottom % steps)) / steps);
      var latTileEnd = ((bbox.top - (bbox.top % steps)) / steps);

      return {"long": {"start":longTileStart, "end": longTileEnd}, "lat": {"start":latTileStart, "end": latTileEnd}, "zoom": bbox.level};
    };

    // creates an 3 dimensional array
    // first dimension holds the admin level
    // second dimension holds the longitude level
    // third dimension holds the latitude level
    // example tiles[0][0][0] shows the most lowest and most left tile on admin level 0
    // each tile contains an object with
      // contents --> holds an array of ids of shapes within this tile
      // rect --> holds the bounding box of this tile (with zoom level)
      // loaded --> boolean that knows if this tile has been loaded or not
      // index --> the index in the tile array
      // loadTiles --> holds an array of tiles with geoInformation to load
    // each admin level has a different amount of tiles
      // the size of the tiles and therefore the amount of tiles per admin level is defined in settings.map.tileSize
    // the tiles array holds admin level 0 to 3
    function createTiles(){
      tiles = [];
      for(var level = 0; level <= choro.settings.map.highestAdminLevel; level++){
        tiles[level] = [];
        for(var long = 0; long <= (360 / choro.settings.map.tileSize[level]); long++){
          tiles[level][long] = [];
          for(var lat = 0; lat <= (180 / choro.settings.map.tileSize[level]); lat++){
            tiles[level][long][lat] = {
              "contents": [],
              "geoTiles": [],
              "rect": {
                "left": long * choro.settings.map.tileSize[level] - 180,
                "right": long * choro.settings.map.tileSize[level] - (180 - choro.settings.map.tileSize[level]),
                "top": lat * choro.settings.map.tileSize[level] - (90 - choro.settings.map.tileSize[level]),
                "bottom": lat * choro.settings.map.tileSize[level] - 90,
                "level": level
              },
              "loaded": false,
              "index": [level, long, lat]
            };
          }
        }
      }
      return tiles;
    }
  }

  // object to store taskobjects
  // an taskobject is used to store information about what data to display at a moment
  // adds an taskRef variable to return the referenced object at a later moment.
  // removes taskRef variable from task and task object from list if the object is requested again.
  choro.TaskQueue = function(){
    var lastRef = -1;

    var queue = [];

    // inserts the task into the queue
    // adds a taskRef variable for identification (every taskRef variable is unique within one TaskQueue)
    // returns the given reference
    this.setTask = function(task){
      task.taskRef = ++lastRef;
      queue.push(task);
      return lastRef;
    };

    // returns the task with the given reference from the queue
    // removes the task from the queue
    // removes the reference variable from the task object
    // returns -1 if the task wasnt found
    this.getTask = function(ref){
      for (var i = queue.length - 1; i >= 0; i--) {
        if(queue[i].taskRef === ref){
          var element = queue.splice(i, 1)[0];

          // remove the taskRef attribute from the object
          delete element.taskRef;

          return element;
        }
      }
      return -1;
    };
  };


  // div debug functions
  choro.Debugger = {
    showTiles: function(level){
      if(!choro.settings.debug.debugMode){return;}

      var bbox = {
        "left": -180,
        "right": 180,
        "top": 90,
        "bottom": -90,
        "level": level
      };

      var steps = choro.settings.map.tileSize[bbox.level];

      // translate the points so the bounding box holds intervals from 0-360 and 0-180
      bbox.left += 180;
      bbox.right += 180;
      bbox.bottom += 90;
      bbox.top += 90;

      //calculates the tile start and end Indices
      var longTileStart = ((bbox.left - (bbox.left % steps)) / steps);
      var longTileEnd = ((bbox.right - (bbox.right % steps)) / steps);

      var latTileStart = ((bbox.bottom - (bbox.bottom % steps)) / steps);
      var latTileEnd = ((bbox.top - (bbox.top % steps)) / steps);

      var bounds = {"long": {"start":longTileStart, "end": longTileEnd}, "lat": {"start":latTileStart, "end": latTileEnd}, "zoom": bbox.level};

      var tiles = choro.tileManager.getTiles(bounds);

      var deb = d3.select("#debug");
      deb.selectAll("*").remove();

      for (var i = 0; i < tiles.length; i++) {
        var coords1 = projection([tiles[i].rect.left, tiles[i].rect.top]);
        var coords2 = projection([tiles[i].rect.right, tiles[i].rect.bottom]);
        if(coords1[0] === Infinity || coords1[1] === Infinity || coords2[0] === Infinity || coords2[1] === Infinity){continue;}
        if(isNaN(coords1[0]) || isNaN(coords1[1]) || isNaN(coords2[0]) || isNaN(coords2[1])){continue;}

        deb.append("rect")
          .attr({
            x: coords1[0],
            y: coords1[1],
            width: Math.abs(coords1[0] - coords2[0]),
            height: Math.abs(coords1[1] - coords2[1]),
            class: "tile"

          })
          .attr("data-tile", tiles[i].index)
          .attr("vector-effect", "non-scaling-stroke")
          .on("click", function(e){
            var t = this.getAttribute("data-tile");
            t = t.split(",");
            var x = choro.tileManager.tilesDebug();
            console.log(x[t[0]][t[1]][t[2]]);
          });
      }
    }
  };

  return choro;

})();

// settings can be changed here before the choro object is initialized

// c.settings.debug.debugMode = true;
// c.settings.data.useLocalstorage = false;
// c.settings.data.updateScale = true;
c.settings.data.colorScale = 0;

// initialize the application
c = c.init();
